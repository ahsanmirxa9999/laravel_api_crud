<?php

namespace App\Http\Controllers;

use App\Models\StudentModel;
use Illuminate\Http\Request;

class student extends Controller
{

    public function create(Request $request)
    {
        $create=  StudentModel::create([
            'name'=>$request->name,
            'lastname'=>$request->lastname,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'address'=>$request->address,
            'birthday'=>$request->birthday,
        ]);
        if ($create) {
            dd('student is created');
        }
    }

    public function show(){
       $data =StudentModel::all();
       return response()->json([
            'status'=>200,
            'data'=>$data
       ]);
    }

    public function table_data($id){
        $data = StudentModel::find($id);
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);
    }

    public function update(Request $request, $id) {
        $student = StudentModel::find($id);

        if (!$student) {
            return response()->json(['error' => 'Student not found'], 404);
        }

        $student->name = $request->input('name');
        $student->lastname = $request->input('lastname');
        $student->phone = $request->input('phone');
        $student->email = $request->input('email');
        $student->address = $request->input('address');
        $student->birthday = $request->input('birthday');

        $student->save();

        return response()->json(['message' => 'Student Updated Successfully']);
    }

}
