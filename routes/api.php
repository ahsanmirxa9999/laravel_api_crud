<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\apicontroller;
use App\Http\Controllers\student;


Route::get('/getStudent',[student::class,'show']);
Route::post('/createStudent',[student::class,'create']);
Route::get('/getStudent/{id}/table_data',[student::class,'table_data']);
Route::put('/createStudent/{id}/update',[student::class,'update']);
